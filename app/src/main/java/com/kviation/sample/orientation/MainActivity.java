package com.kviation.sample.orientation;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager sensorManager;
    private Sensor accelerometer;
    private Sensor gyroscope;
    double a_x, a_y, a_z;
    double r_x, r_y, r_z;
    TextView a1;
    TextView a2;
    TextView a3;
    TextView g1;
    TextView g2;
    TextView g3;
    DecimalFormat df;
    String temp_data_a = "";
    String temp_data_g = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);


        df = new DecimalFormat();
        df.setMaximumFractionDigits(4);

        a1 = (TextView) findViewById(R.id.textViewa1);
        a2 = (TextView) findViewById(R.id.textViewa2);
        a3 = (TextView) findViewById(R.id.textViewa3);
        g1 = (TextView) findViewById(R.id.textViewg1);
        g2 = (TextView) findViewById(R.id.textViewg2);
        g3 = (TextView) findViewById(R.id.textViewg3);

        a1.setText(String.valueOf("X-axis: 0.0"));
        a2.setText(String.valueOf("Y-axis: 0.0"));
        a3.setText(String.valueOf("Z-axis: 0.0"));
        g1.setText(String.valueOf("X-axis: 0.0"));
        g2.setText(String.valueOf("Y-axis: 0.0"));
        g3.setText(String.valueOf("Z-axis: 0.0"));

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        gyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

        Button start = (Button) findViewById(R.id.button);
        Button stop = (Button) findViewById(R.id.button2);
        Button view = (Button) findViewById(R.id.button3);
        Button delete = (Button) findViewById(R.id.button4);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startVisualisation();
            }
        });
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopVisualisation();
                a1.setText(String.valueOf("X-axis: 0.0"));
                a2.setText(String.valueOf("Y-axis: 0.0"));
                a3.setText(String.valueOf("Z-axis: 0.0"));
                g1.setText(String.valueOf("X-axis: 0.0"));
                g2.setText(String.valueOf("Y-axis: 0.0"));
                g3.setText(String.valueOf("Z-axis: 0.0"));
            }
        });
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TAG_A", String.valueOf(temp_data_a.length()));
                Log.d("TAG_G", String.valueOf(temp_data_g.length()));
                writeToFile(temp_data_a, "acce");
                writeToFile(temp_data_g, "gyro");
                Toast t = Toast.makeText(getApplicationContext(), "Stored data in internal memory /WARDI", Toast.LENGTH_SHORT);
                t.show();
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                temp_data_g = "";
                temp_data_a = "";
            }
        });
    }

    public void writeToFile(String data, String name) {
        File sdcard = Environment.getExternalStorageDirectory();
        File dir = new File(sdcard.getAbsolutePath() + "/WARDI/");
        dir.mkdir();
        Log.d("TAG_llllllll", String.valueOf(data.length()));
        File file = new File(dir, name + "_data.txt");
        FileOutputStream os = null;
        try {
            os = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            os.write(data.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
//
//        try
//        {
//            file.createNewFile();
//            FileOutputStream fOut = new FileOutputStream(file);
//            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
//            myOutWriter.append(data);
//
//            myOutWriter.close();
//
//            fOut.flush();
//            fOut.close();
//        }
//        catch (IOException e)
//        {
//            Log.e("Exception", "File write failed: " + e.toString());
//        }
    }

    private void startVisualisation() {
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, gyroscope, SensorManager.SENSOR_DELAY_NORMAL);
    }

    private void stopVisualisation() {
        sensorManager.unregisterListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            a_x = event.values[0];
            a1.setText(String.valueOf("X-axis: " + df.format(a_x)));
            a_y = event.values[1];
            a2.setText(String.valueOf("Y-axis: " + df.format(a_y)));
            a_z = event.values[2];
            a3.setText(String.valueOf("Z-axis: " + df.format(a_z)));
            temp_data_a += String.valueOf(df.format(a_x) + ", " + df.format(a_y) + ", " + df.format(a_z) + "\n");
        } else if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
            r_x = event.values[0];
            g1.setText(String.valueOf("X-axis: " + df.format(r_x)));
            r_y = event.values[1];
            g2.setText(String.valueOf("Y-axis: " + df.format(r_y)));
            r_z = event.values[2];
            g3.setText(String.valueOf("Z-axis: " + df.format(r_z)));
            temp_data_g += String.valueOf(df.format(r_x) + ", " + df.format(r_y) + ", " + df.format(r_z) + "\n");
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    Toast.makeText(MainActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}

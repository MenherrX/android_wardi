package com.kviation.sample.orientation;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.Highlight;

import java.util.ArrayList;
import java.util.Random;

public class Example extends AppCompatActivity implements OnChartValueSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.example);
        BarChart chart1 = (BarChart) findViewById(R.id.chart1);
        BarChart chart2 = (BarChart) findViewById(R.id.chart2);

        chart2.getAxisLeft().setStartAtZero(false);
        chart1.getAxisRight().setStartAtZero(false);
        chart1.getAxisLeft().setStartAtZero(false);
        chart2.getAxisRight().setStartAtZero(false);


        BarData data1 = new BarData(getXAxisValues(), getDataSet());
        BarData data2 = new BarData(getXAxisValues(), getDataSet1());

        chart1.setOnChartValueSelectedListener(this);
        chart1.setData(data1);
        chart1.notifyDataSetChanged();
        chart1.animateXY(0, 2000);
        chart1.invalidate();




        chart2.setData(data2);
        chart2.notifyDataSetChanged();
//        chart2.setDescription("Gyroscope");
        chart2.animateXY(0, 2000);
        chart2.invalidate();
//        data1.addEntry(new Entry(14.020f, 0),0);
//        chart1.notifyDataSetChanged(); // let the chart know it's data changed
//        chart1.invalidate();
    }


    private ArrayList<BarDataSet> getDataSet() {
        ArrayList<BarDataSet> dataSets = null;
        Random r = new Random();
        float a = (r.nextFloat() * -40) + 20;
        float b = (r.nextFloat() * -40) + 20;
        float c = (r.nextFloat() * -40) + 20;
        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        BarEntry v1e1 = new BarEntry(a, 0);
        valueSet1.add(v1e1);
        BarEntry v1e2 = new BarEntry(b, 1);
        valueSet1.add(v1e2);
        BarEntry v1e3 = new BarEntry(c, 2);
        valueSet1.add(v1e3);

        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "Accelerometer Visualize");
        barDataSet1.setColors(ColorTemplate.COLORFUL_COLORS);

        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
        return dataSets;
    }

    private ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("X-AXIS");
        xAxis.add("Y-AXIS");
        xAxis.add("Z-AXIS");
        return xAxis;
    }

    private ArrayList<BarDataSet> getDataSet1() {
        ArrayList<BarDataSet> dataSets = null;

        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        BarEntry v1e1 = new BarEntry(14.020f, 0);
        valueSet1.add(v1e1);
        BarEntry v1e2 = new BarEntry(-40.000f, 1);
        valueSet1.add(v1e2);
        BarEntry v1e3 = new BarEntry(10.0f, 2);
        valueSet1.add(v1e3);

        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "Gyroscope Visualize");
        barDataSet1.setColors(ColorTemplate.COLORFUL_COLORS);

        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
        return dataSets;
    }



    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }
}